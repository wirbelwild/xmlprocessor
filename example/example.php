<?php

/**
 * This example needs a data.xml file containing some nodes called `item` with some child nodes called `listitem`.
 * You can download a huge dataset from here: http://www.ins.cwi.nl/projects/xmark/Assets/standard.gz
 *
 * Here we measure the time to read the xml data and to parse it
 */

use BitAndBlack\Measurement\Measurement;
use BitAndBlack\Measurement\Unit;
use BitAndBlack\XML\XMLProcessor;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Output\ConsoleOutput;

require dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

$measurement = new Measurement();
$output = new ConsoleOutput();
$output->writeln('Starting test');

/**
 * Part 1
 * Reading the file
 */

$unit1 = new Unit('Reading file');

$measurement->add(
    $unit1->start()
);

$output->writeln('Starting '.$unit1->getDescription());

if (!file_exists('data.xml')) {
    die('Missing data.xml file. If you want to download a huge file, consider this one: http://www.ins.cwi.nl/projects/xmark/Assets/standard.gz' . PHP_EOL);
}

$domDocument = new DOMDocument();
$domDocument->load('data.xml');

$output->writeln('Ended '.$unit1->getDescription());

$unit1->end();

/**
 * Part 2
 * The way we do with XMLProcessor
 */

$unit2 = new Unit('EachElement with XML Processor');

$measurement->add(
    $unit2->start()
);

$output->writeln('Starting '.$unit2->getDescription());

$items = null;
$progress = new ProgressBar($output);

$processor = new XMLProcessor($domDocument);
$processor->eachElement(
    static function(DOMDocument $domDocument) use ($output, $items, &$progress) {
        $items = $domDocument->getElementsByTagName('item');
        $itemsCount = count($items);
        $output->writeln('Found '.$itemsCount.' nodes.');
        $progress->setMaxSteps($itemsCount);
        return $items;
    },
    static function($node, $key) use (&$progress) {
        $progress->advance();
    }
);

$output->writeln('');
$output->writeln('Ended '.$unit2->getDescription());

$unit2->end();

unset($processor);

/**
 * Part 3
 * The classical way with foreach loops
 */

$unit3 = new Unit('Standard Foreach');

$measurement->add(
    $unit3->start()
);

$output->writeln('Starting '.$unit3->getDescription());

$items = $domDocument->getElementsByTagName('item');
$progress = new ProgressBar($output);
$itemsCount = count($items);
$output->writeln('Found '.$itemsCount.' nodes.');
$progress->setMaxSteps($itemsCount);

/**
 * @var DOMNode $item
 */
foreach ($items as $item) {
    $progress->advance();
}

$output->writeln('');
$output->writeln('Ended '.$unit3->getDescription());

$unit3->end();

$rightAligned = new TableStyle();
$rightAligned->setPadType(STR_PAD_LEFT);

$table = new Table($output);
$table
    ->setHeaders(['Process Description', 'Time in Seconds'])
    ->setStyle('box')
    ->setColumnStyle(1, $rightAligned)
;

foreach ($measurement->getUnits() as $unit) {
    $table->addRow([
        $unit->getDescription(), 
        number_format($unit->getTime(), 2)
    ]);
}

$measurement->createSummary();
$table->render();

unset($domDocument);