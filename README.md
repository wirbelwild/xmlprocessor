[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/xmlprocessor)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/c9bff9d0c6c247e0ba0e46a1d76090f1)](https://www.codacy.com/bb/wirbelwild/xmlprocessor/dashboard) 
[![Latest Stable Version](https://poser.pugx.org/bitandblack/xmlprocessor/v/stable)](https://packagist.org/packages/bitandblack/xmlprocessor)
[![Total Downloads](https://poser.pugx.org/bitandblack/xmlprocessor/downloads)](https://packagist.org/packages/bitandblack/xmlprocessor)
[![License](https://poser.pugx.org/bitandblack/xmlprocessor/license)](https://packagist.org/packages/bitandblack/xmlprocessor)

# XMLProcessor

Fastest handling of huge XML data. 

The Bit&Black XML Processor is up to **20 times faster** in parsing huge xml data.
This is because it doesn't make use of `foreach` to handle XML nodes. Instead, the processor uses a `while` loop and a copy of the DOMElements.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/xmlprocessor). Add it to your project by running `$ composer require bitandblack/xmlprocessor`.

## Usage

Set up the processor with your xml data:

```php
<?php

use DOMDocument;
use BitAndBlack\XML\XMLProcessor;

$domDocument = new DOMDocument();
$domDocument->load('your_huge_file.xml');

$xmlProcessor = new XMLProcessor($domDocument);
```

Use `eachElement()` now instead of `foreach`:

```php
<?php

//$nodes = $domDocument->getElementsByTagName('some_node'):
//
//foreach ($nodes as $key => $node) {
//    var_dump('Found at position ' . $key . ' node with id ' . $node->getAttribute('id'));
//}

$xmlProcessor->eachElement(
    static function(DOMDocument $domDocument) {
        return $domDocument->getElementsByTagName('some_node');
    },
    static function(DOMNode $node, int $key) {
        var_dump('Found at position ' . $key . ' node with id ' . $node->getAttribute('id'));
    }
);
```

The first attribute is a function where you can set up the statement with whom you can select the nodes you want to.

The second attribute is a callback function which is called for every processed item.

`eachElement()` makes a clone of the DOM to reach a fast processing so modifying the original DOM at the same time will not target the processed data. 

## Help 

If you have questions feel free to contact us under `hello@bitandblack.com`. 

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).