<?php

/**
 * XMLProcessor
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\XML\Exception;

use BitAndBlack\XML\Exception;

class WrongDocumentException extends Exception
{
    /**
     * @param string $inputType
     */
    public function __construct(string $inputType)
    {
        parent::__construct('Expected object of type "DOMNode" or "DOMNodeList", but received "' . $inputType . '" instead.');
    }
}
