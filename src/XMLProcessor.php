<?php

/**
 * XMLProcessor
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\XML;

use BitAndBlack\XML\Exception\WrongDocumentException;
use DOMDocument;
use DOMNode;
use DOMNodeList;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class XMLProcessor
 *
 * @package BitAndBlack\XML
 */
class XMLProcessor implements LoggerAwareInterface
{
    /**
     * @var DOMNode|DOMNodeList<DOMNode>
     */
    private $domDocument;

    private LoggerInterface $logger;

    /**
     * XMLProcessor constructor.
     *
     * @param DOMNode|DOMNodeList<DOMNode> $domDocument
     * @throws WrongDocumentException
     */
    public function __construct($domDocument)
    {
        if (!$domDocument instanceof DOMNode && !$domDocument instanceof DOMNodeList) {
            $inputType = gettype($domDocument);
            throw new WrongDocumentException($inputType);
        }

        $this->domDocument = $domDocument;
        $this->logger = new NullLogger();
    }

    /**
     * Sets a logger instance
     *
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
        $this->logger->debug('Init XMLProcessor');
    }

    /**
     * Cloning the DOM
     *
     * @return DOMNode
     */
    private function getDomDocTemp(): DOMNode
    {
        /**
         * If dataset is an instance of DOMDocument we simply clone everything
         * This is also the fastest way
         */
        if ($this->domDocument instanceof DOMDocument) {
            $domDocTemp = clone $this->domDocument;
            $this->logger->debug('Elements cloned');
            return $domDocTemp;
        }

        /**
         * If dataset is an instance of DOMNodeList we need the while to process every node
         */
        if ($this->domDocument instanceof DOMNodeList) {
            $this->logger->debug('Elements moved because of DOMNodeList');

            $domDocTemp = new DOMDocument();
            $length = $this->domDocument->length;
            $counter = 0;

            while ($counter < $length) {
                $item = $this->domDocument->item($counter);
                ++$counter;

                if (null === $item) {
                    continue;
                }

                $nodeImported = $domDocTemp->importNode($item, true);
                $domDocTemp->appendChild($nodeImported);
            }

            return $domDocTemp;
        }

        /**
         * We received a single DOMNode
         */
        $this->logger->debug('Elements moved because of DOMNode');

        $domDocTemp = new DOMDocument();

        $domDocTemp->appendChild(
            $domDocTemp->importNode($this->domDocument, true)
        );

        return $domDocTemp;
    }

    /**
     * Processing each element of a DOM
     *
     * @param callable $getElementStatement
     * @param callable $callback
     * @return void
     */
    public function eachElement(callable $getElementStatement, callable $callback): void
    {
        $domDocTemp = $this->getDomDocTemp();

        /**
         * @var DOMNodeList<DOMNode> $nodes
         */
        $nodes = $getElementStatement($domDocTemp);

        /**
         * The while allows a fast handing if the tree
         * To enable this, we need to remove every processed item
         */
        $element = $nodes->item(0);

        $key = 0;

        while (null !== $element) {
            /**
             * Running the callback
             *
             * @var DOMNode $element
             * @var int $key
             */
            $callback($element, $key);

            ++$key;
            
            $first = $element;
            
            if (null !== $first->parentNode) {
                $first->parentNode->removeChild($first);
            }

            $element = $nodes->item(0);
        }
    }
}
