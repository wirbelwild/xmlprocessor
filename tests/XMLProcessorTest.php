<?php

/**
 * XMLProcessor
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\XML\Tests;

use BitAndBlack\XML\Exception\WrongDocumentException;
use BitAndBlack\XML\XMLProcessor;
use DOMDocument;
use DOMNode;
use DOMNodeList;
use PHPUnit\Framework\TestCase;

/**
 * Class XMLProcessorTest
 *
 * @package BitAndBlack\XML\Tests
 */
class XMLProcessorTest extends TestCase
{
    /**
     * @var DOMNodeList<DOMNode>
     */
    private $testNodesOriginal;

    private string $testXML = '<?xml version="1.0"?><root><test>First node value</test><test>Second node value</test></root>';

    /**
     * Tests if processed node values stay the same.
     *
     * @throws WrongDocumentException
     */
    public function testEqualNodeValues(): void
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($this->testXML);

        $this->testNodesOriginal = $domDocument->getElementsByTagName('test');

        $processor = new XMLProcessor($domDocument);
        $processor->eachElement(
            static fn (DOMDocument $domDocument) => $domDocument->getElementsByTagName('test'),
            function (DOMNode $node, int $key) {
                $this->equalNodeValues($node, $key);
            }
        );
    }

    /**
     * @param DOMNode $nodeProcessed
     * @param int $key
     */
    private function equalNodeValues(DOMNode $nodeProcessed, int $key): void
    {
        $nodeOriginal = $this->testNodesOriginal->item($key);

        self::assertNotNull(
            $nodeOriginal
        );
        
        if (null === $nodeOriginal) {
            return;
        }
        
        self::assertSame(
            $nodeOriginal->nodeValue,
            $nodeProcessed->nodeValue
        );
    }

    /**
     * Tests if processing uses duplication of nodes.
     *
     * @throws WrongDocumentException
     */
    public function testDOMStaysUntouched(): void
    {
        $domDocument1 = new DOMDocument();
        $domDocument1->loadXML($this->testXML);

        $domDocument2 = new DOMDocument();
        $domDocument2->loadXML($this->testXML);

        $processor = new XMLProcessor($domDocument1);
        $processor->eachElement(
            static fn (DOMDocument $domDocument) => $domDocument->getElementsByTagName('test'),
            static function (DOMNode $node, int $key) {
            }
        );

        self::assertSame(
            $domDocument1->saveXML(),
            $domDocument2->saveXML()
        );
    }

    /**
     * @throws WrongDocumentException
     */
    public function testCanHandleNodeList(): void
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($this->testXML);

        $testElements = $domDocument->getElementsByTagName('test');

        self::assertCount(
            2,
            $testElements
        );

        $elementCounter = 0;
        $elementValues = [];

        $processor = new XMLProcessor($testElements);
        $processor->eachElement(
            static fn (DOMDocument $domDocument) => $domDocument->getElementsByTagName('test'),
            static function (DOMNode $node, int $key) use (&$elementCounter, &$elementValues) {
                ++$elementCounter;
                $elementValues[$key] = $node->nodeValue;
            }
        );

        self::assertSame(
            2,
            $elementCounter
        );

        self::assertSame(
            [
                0 => 'First node value',
                1 => 'Second node value'
            ],
            $elementValues
        );
    }

    public function testThrowsException(): void
    {
        $this->expectException(WrongDocumentException::class);
        $this->expectExceptionMessage('Expected object of type "DOMNode" or "DOMNodeList", but received "string" instead.');

        /** @phpstan-ignore-next-line */
        new XMLProcessor('Hello world');
    }
}
